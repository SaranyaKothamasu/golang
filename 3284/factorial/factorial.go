// factorial of the number given as input
package main

import "fmt"

func main() {
	var num int
	var factorial int
	factorial = 1
	fmt.Print("Enter any Number to find the Factorial = ")
	fmt.Scanln(&num)
	for i := 1; i <= num; i++ {
		factorial = factorial * i
	}
	fmt.Println("The Factorial of ", num, " = ", factorial)
}
