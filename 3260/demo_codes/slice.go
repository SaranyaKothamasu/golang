//learning slice

package main

import "fmt"

func main() {
	array1 := [5]int{1, 2, 3}
	slice1 := []int{1, 2}

	fmt.Println(array1)
	fmt.Println(slice1)

	slice1 = append(slice1, 6)
	fmt.Println(slice1)

	fmt.Println("slice[1:2]", slice1[1:2])

	slice1 = slice1[2:3]
	fmt.Print(slice1)
}
