
// This is a program which takes number as input which computes factorial of that number


package main

import "fmt"

func main() {

    fmt.Println("Enter the Number")
    var n int
    fmt.Scanln(&n)
    var ans int = printFactorial(n)

    fmt.Println(ans)

}

func printFactorial(n int) int {

    if n == 1 || n==0 {
        return 1
    }

    return n * printFactorial(n-1)

}
