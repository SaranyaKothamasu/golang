// This program calculates the factorial of number given by a user.
package main

import "fmt"

func main() {

	var num, ans int
	ans = 1

	fmt.Print("Enter the input:")
	fmt.Scanln(&num)

	for i := 1; i <= num; i++ {
		ans = ans * i
	}
	fmt.Println(ans)
}
