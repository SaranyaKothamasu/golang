package main

import "fmt"

func factorial(n int) int {
	if n == 0 {
		return 0
	}
	var ans = 1
	for i := 1; i <= n; i++ {
		ans *= i
	}
	return ans
}
func main() {

	var fact = factorial(10)
	fmt.Println(fact)
}
