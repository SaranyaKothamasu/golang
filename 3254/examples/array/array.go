// experimenting with arrays

package main

import "fmt"

func main() {
	array1 := [5]int{4, 5, 7}
	array2 := [5]int{4, 5, 7}
	fmt.Println(array1)
	fmt.Println(array2)
}
